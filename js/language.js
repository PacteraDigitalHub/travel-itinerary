 
function checkCookie(){
	//alert("checkCookie")
	var lang=getCookie("lang_flag");
	if (lang!=""){
		//alert("lang in checkCookie"+lang);
	}
	else {
		//alert("@@@@@"+lang)
		setCookie("lang_flag","1",30);
    	lang=getCookie("lang_flag");
	}
	return lang;
}
function getCookie(cname){
	//alert("getCookie")
	//alert("cname:"+cname)
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {

		var c = ca[i].trim();
		//alert(c);
		
		if (c.indexOf(name)==0) { return c.substring(name.length,c.length); }
	}
	return "";
}

function setCookie(cname,cvalue,exdays){
	//alert("setCookie")
	//alert("cvalue:"+cvalue+"cname:"+cname);
	var d = new Date();
	d.setTime(d.getTime()+(exdays*24*60*60*1000));
	var expires = "expires="+d.toGMTString();
	var path = "path=/";
	document.cookie = cname+"="+cvalue+"; "+expires+";"+path;
	//alert("document.cookie:"+document.cookie);
}

function changeLanguage(){
		var lang_flag = checkCookie(lang_flag);
		alert("changeLanguage"+lang_flag);
		if(lang_flag == 1){
			lang_flag = 0;
		}
		else {
			lang_flag = 1;
		}
		setCookie("lang_flag",lang_flag,30)
		var txts= document.querySelectorAll("[id^=txt]");
		
		txts.forEach( function(currentValue, currentIndex, listObj){
			//alert(currentValue.id + currentValue.innerHTML);
			document.getElementById(currentValue.id).innerText = json[currentValue.id][lang_flag];
		});
		
		document.getElementById("switch").innerHTML = lang_type[lang_flag];
		
}
function imageCarousal() {
	
	var engSlider = document.getElementById("engSlider");
	var chiSlider =	document.getElementById("chiSlider");
	var englSlider = document.getElementById("englSlider");
	var chinSlider = document.getElementById("chinSlider");
	
	if (engSlider) {
		// alert('Hey');
		chiSlider.style.display = 'none';
		engSlider.style.display = 'block';
	} 

	if (chiSlider) {
		engSlider.style.display = 'none';
		chiSlider.style.display = 'block';
	}
	if (englSlider) {
		chinSlider.style.display = 'none';
		englSlider.style.display = 'block';
	} 

	if (chinSlider) {
		englSlider.style.display = 'none';
		chinSlider.style.display = 'block';
	}

}

function onload(){
		var lang_flag = 0;//checkCookie(lang_flag);
		//alert("changeLanguage"+lang_flag);
		setCookie("lang_flag",lang_flag,30)
		var txts= document.querySelectorAll("[id^=txt]");
		
		txts.forEach( function(currentValue, currentIndex, listObj){
			document.getElementById(currentValue.id).innerText = json[currentValue.id][lang_flag];
		});
		document.getElementById("switch").innerHTML = lang_type[lang_flag];
		
}
var lang_type = ["中文", "English"]
var json = {
  "txtWelcome": ["Welcome Kar Yee", "欢迎 Kar Yee"],
  "txtWelcomeInfo": [
    "Ahead of your upcoming visit to Shenzhen, we have prepared some information for you about Pactera and our local operations, as well as the city itself – its history, places of interest and of course where to eat. We have proposed an itinerary for your visit which we hope you find acceptable. Please browse via the links below. We’re excited to be able to host you at our Shenzhen delivery centre and keen to ensure that you get the most out of your time with us. To that end, if there is anything particular you want to know, see or do, please contact either David or Deon directly and we’ll make it happen.",
    "欢迎您莅临深圳！我们为您的访问制定了一个行程，包括文思海辉、当地业务以及深圳历史、名胜、美食的相关信息， 希望您能够喜欢。请点击以下链接了解详细信息。我们很高兴能够在深圳交付中心接待您，并真诚地希望您能够享受本次旅程。如果您想知道任何其他信息，或者其他想参观的景点，请与David或Deon联系，我们将帮助您制定行程。"
  ],
  "txtSession1": ["About Shenzhen","关于深圳"],
  "txtSession1info": ["Visit This Place","参观此地"],
  "txtAboutUs": ["About Pactera","关于Pactera"],
  "txtAboutPactera":["About Pactera","关于Pactera"],
  "txtItinerary": ["Itinerary","日程安排"],
  "txtLocalContacts": ["Local Contacts","本地联系人"],
  "txtLinks": ["Links","相关链接"],
  "txtLink0": ["Home","主页"],
  "txtLink1": ["About Pactera","关于Pactera"],
  "txtLink2": ["About Shenzhen","关于深圳"],
  "txtLink3": ["Itinerary","日程安排"],
 "txtNews":["Latest Pactera News","文思海辉新闻资讯"],
 "txtAboutShenzhen":["About Shenzhen","关于深圳"],
 "txtDestinationTopic1":["Shopping Malls","购物中心"],
 "txtDestinationTopic2":["Theatre","剧院"],
 "txtDestinationTopic3":["Parks","主题公园"],
 "txtDestinationTopic4":["Sightseeing","观光景点"],
 "txtSeeMore1":["Visit This Place","参观此地"],
 "txtSeeMore2":["Visit This Place","参观此地"],
 "txtSeeMore3":["Visit This Place","参观此地"],
 "txtSeeMore4":["Visit This Place","参观此地"],
 
 "txtToppic1Info":["Shenzhen is a fast developing modern city in China. Finding large shopping malls here is an easy thing. Shenzhen has a variety of shopping malls, from luxury ones to popular and civic ones. We would like to introduce some top shopping malls in Shenzhen for your reference. Hope you can have a good shopping experience.",
 "深圳是中国快速发展的现代化城市之一， 大型购物中心林立。 从当地特色商城、流行潮店到奢侈品购物中心应有尽有。在此，我们介绍一些购物中心供您参考。 祝您购物愉快。"],
 "txtToppic2Info":["Shenzhen Grand Theatre is one of the eight famous cultural facilities in Shenzhen City. It is a multifunctional, modern, and comprehensive cultural site combining arts, song and dance shows, cultural tours and leisure. ",
 "深圳大剧院是深圳市八大著名文化设施之一。 这是一个集艺术、歌舞表演、文化旅游和休闲等多功能于一体的现代化、综合性文化场所。"],
 "txtToppic3Info":["Shenzhen as a young city has a lot of entertainment to offer to young people, and the most impressive one is the number and variety of theme parks. This also makes Shenzhen a great place for families with young children to live because you can never run out of ideas of where to take the kids to.",
 "深圳是一个年轻的城市，为年轻人提供了很多娱乐活动场所，最令人印象深刻的是主题公园的数量和种类。 这也使深圳成为一个有孩子家庭的宜居城市， 因为你永远不用担心没地方玩"],
 "txtToppic4Info":["Shun Hing Square, also known as Di Wang Tower， is a 384-metre-tall (1,260 ft) skyscraper in Shenzhen.",
 "地王大厦正式名称为信兴广场，是一座摩天大楼。因信兴广场所占土地当年拍卖拍得深圳土地交易最高价格，被称为“地王”，因此公众称之为地王大厦。大厦高69层，总高度383.95米，实高324.8米，建成时是亚洲第一高楼，现在是深圳第一高楼，也是全国第一个钢结构高层建筑，位居目前世界十大建筑之列。大楼于1996年完工。"],
"txtShopName1":["Shenzhen MIXC City","万象城"],
 "txtShopName2":["Yitian Holiday Plaza","益田假日广场"],
 "txtShopName3":["Lo Wu Shopping Plaza","罗湖商业城"],
 "txtShopName4":["King Glory Plaza","金光华广场"],
 "txtShopName5":["CoastalCity Shopping Centre","海岸城购物中心"],
 "txtDestinationTopic1Session":["Shopping Malls","购物中心"],
 "txtDestinationTopic2Session":["Theatres","剧院"],
 "txtDestinationTopic3Session":["Parks","主题公园"],
 "txtDestinationTopic4Session":["Sightseeing","观光景点"],
 "txtTheaterName1":["Shenzhen Grand Theatre","深圳大剧院"],
  "txtTheaterName2":["Poly Theatre","保利大剧院"],
  "txtParkName1":["Shenzhen Window of the World","世界之窗"],
  "txtParkName2":["East Overseas Chinese Town","东部华侨城"],
  "txtParkName3":["Happy Vally","欢乐谷"],
  "txtSiteSeeingName1":["Shun Hing Square","地王大厦"],
  "txtSiteSeeingName2":["China Folk Culture Village","民俗文化村"],
  "txtSiteSeeingName3":["Hong Fa Buddhist Temple","弘法寺"],
  
  
 "txtHeaderDate1":["Tuesday 12 March 2019", "2019年3月13日星期三"],
  "txtHeaderDate2":["Wednesday 13 March 2019", "2019年3月14日星期四"],  
 "txtDay1":["Day 1", "第一日"],
 "txtDay2":["Day 2", "第二日"],
 "txtTable1Coloum1":["Time", "时间"],
 "txtTable1Coloum2":["Activity", "活动"],
 "txtTable1Coloum3":["Attendees", "人员"],
 "txtTable2Coloum1":["Time", "时间"],
 "txtTable2Coloum2":["Activity", "活动"],
 "txtTable2Coloum3":["Attendees", "人员"],
 "txtTable1Coloum1Row1":["18:30 – 22:00","18:30 – 22:00"],
 "txtTable1Coloum2Row1":["Dinner at St Regis Shenzhen – meet at Decanter Bar ","晚餐：深圳瑞吉酒店 集合点：Decanter Bar "],
 "txtTable2Coloum2Row1":["Lunch Venue TBA","午餐地点待定"],
 "txtTable2Coloum2Row2":["Drive to Vision Park","威新软件科技园"],
 "txtTable2Coloum2Row3":["Introduction to Pactera Shenzhen ODC – facilitated by Andy Fung","深圳文思海辉深圳简介 - 由Andy Fung主持"],
 "txtTable2Coloum2Row4":["Expedia ODC tour – facilitated by Ring Huang","Expedia 海外研发中心之旅 - 由Ring Huang主持"],
 "txtTable2Coloum2Row5":["Hong Kong Jockey Club ODC – facilitated by Andy Xiao & Cathy Yang","香港赛马会ODC  - 由Andy Xiao和Cathy Yang主持"],
 "txtTable2Coloum2Row6":["Hutchison Mobile ODC – facilitated by Andy Fung", "和記電訊 移动端ODC  - 由Andy Fung主持"],
 "txtTable2Coloum2Row7":["SSOC (SZ Qianhai) – facilitated by Sophi Li","SSOC（深圳前海） - 由Sophi Li主持"],
"txtTable2Coloum2Row8":["Move to Tencent facility","腾讯工厂"],
"txtTable2Coloum2Row9":["Tencent Facility tour and wrap up","腾讯之旅与总结"],
"txtTable2Coloum2Row10":["Debrief and wrap up at SSOC – facilitated by David Phoung","在SSOC的汇报与总结 - 由David Phoung主持"],
"txtTable2Coloum2Row11":["Drinks – venue TBA","饮料 - 场地待定"],

 
 
 "txtTeam":["Pactera’s leadership team brings a wealth of experience from top global enterprises.","文思海辉的领导团队有着源自全球各大知名品牌的实战经验。我们富有创业精神，鼓励冒险，期待与来自世界各地的同事们分享我们宝贵的技能和经验。"],
 "txtTitle1":["Executive VP BG-9","BG-9 执行副总裁"],
 "txtTitle2":["Senior VP, ASEAN Head at Pactera","ASEAN负责人，高级副总裁"],
 "txtTitle3":["General Manager Hong Kong, SZ, Taiwan & Macau","港台深澳总经理"],
 "txtTitle4":["Managing Director, Australia","澳大利亚常务董事"],
 
 
 "txtAboutSession1":["Pactera Global","全球Pactera"],
 "txtAboutSession2":["Pactera Shenzhen","深圳Pactera"],
 "txtAboutSession3":["Leadership","领导团队"]
 
 
}